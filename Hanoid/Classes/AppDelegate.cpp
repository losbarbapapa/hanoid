#include "AppDelegate.h"
#include "MenuLayer.h"
#include "GameManager.h"
#include "SonarFrameworks.h"

USING_NS_CC;
using namespace std;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("Hanoid");
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    register_all_packages();

	glview->setDesignResolutionSize(960, 640, ResolutionPolicy::EXACT_FIT);
	
    // Donde busco las imagenes?
	vector<string> searchPaths = FileUtils::getInstance()->getSearchPaths();
	searchPaths.push_back("language");
	searchPaths.push_back("music");
    searchPaths.push_back("fonts");
    
	searchPaths.push_back("Scene");
	searchPaths.push_back("Levels");
	searchPaths.push_back("Buttons");
    searchPaths.push_back("Discs");
	searchPaths.push_back("Help");
	FileUtils::getInstance()->setSearchPaths(searchPaths);

	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("game.mp3");

	if(UserDefault::getInstance()->getIntegerForKey(ACTIVE_MUSIC, -1) == 0){
		GameManager::getInstance()->setActiveMusic(false);
		SimpleAudioEngine::getInstance()->playBackgroundMusic("game.mp3", true);
		SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	}else{
		SimpleAudioEngine::getInstance()->playBackgroundMusic("game.mp3", true);
	}

	SimpleAudioEngine::getInstance()->preloadEffect("button.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("cloud_button.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("magic_chime.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("counter.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("counter_ready.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("error.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("game_paused.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("flip.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("discs.wav");
	SimpleAudioEngine::getInstance()->preloadEffect("close_button.wav");
    
	if(UserDefault::getInstance()->getIntegerForKey(ACTIVE_SOUND, -1) == 0){
		GameManager::getInstance()->setActiveSound(false);
	}

	if(strcmp(UserDefault::getInstance()->getStringForKey(LANGUAGE).c_str(), "") != 0){
		GameManager::getInstance()->setLanguage(UserDefault::getInstance()->getStringForKey(LANGUAGE));
	}

    // create a scene. it's an autorelease object
    auto scene = MenuLayer::createScene();

    // run
    director->runWithScene(scene);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    SonarCocosHelper::IOS::Setup();
#endif
    SonarCocosHelper::AdMob::showBannerAd(SonarCocosHelper::AdBannerPosition::eBottom);
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    if(GameManager::getInstance()->getActiveMusic()){
    	// if you use SimpleAudioEngine, it must resume here
    	SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    }
}
