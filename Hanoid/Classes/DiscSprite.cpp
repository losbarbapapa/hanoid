#include "DiscSprite.h"

USING_NS_CC;

DiscSprite::DiscSprite() {}

DiscSprite::~DiscSprite() {}

DiscSprite* DiscSprite::create()
{
    DiscSprite* pSprite = new DiscSprite();
    
    if (pSprite->init())
    {
        pSprite->autorelease();
        
        return pSprite;
    }
    
    CC_SAFE_DELETE(pSprite);
    return NULL;
}