#ifndef __DISC_SPRITE_H__
#define __DISC_SPRITE_H__

#include "cocos2d.h"

class DiscSprite : public cocos2d::Sprite
{
public:
    cocos2d::Point myPos;
    
    DiscSprite();
    ~DiscSprite();
    static DiscSprite* create();
};

#endif // __DISC_SPRITE_H__
