#include "GameManager.h"
#include <string>

using std::string;
USING_NS_CC;

static GameManager* gameManager = nullptr;

GameManager::GameManager()
{
    ActiveSound = true;
    ActiveMusic = true;
    CurrentLevel = 1;
    GameOverStars = 0;
    GameOverScore = 0;

    string storedLang = UserDefault::getInstance()->getStringForKey("language");
    string appLang = Application::getInstance()->getCurrentLanguageCode();
    
    if(strcmp(storedLang.c_str(), "") == 0){
        if(strcmp(appLang.c_str(), "en") == 0){
            Language = "en"; // english language
        }else if(strcmp(appLang.c_str(), "es") == 0){
            Language = "es"; // spanish language
        }else{
            Language = "en"; // default
        }
    }else
        Language = storedLang;
}

GameManager* GameManager::getInstance()
{
    if (!gameManager) {
        gameManager = new (std::nothrow)GameManager();
    }

    return gameManager;
}
