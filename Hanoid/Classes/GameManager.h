#ifndef _GameManager_H_
#define _GameManager_H_

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include <string>
using std::string;

using namespace CocosDenshion;

#define BAR1_X 172.5
#define BAR2_X 480
#define BAR3_X 787.5

#define DISCS_HEIGHT 32
#define BARS_Y  86
#define SUN_Y   500
#define AZ_Y    260
#define DISCS_SEP   0
#define TRANS_SPEED 0.6
#define FORTE_FONT "forte.ttf"

// Usado en UserDefault
#define ACTIVE_SOUND "ACTIVE_SOUND"
#define ACTIVE_MUSIC "ACTIVE_MUSIC"
#define LANGUAGE "LANGUAGE"
#define LEVEL_SCORE "level%i_score%i"
#define LEVEL_STARS "level%i_stars%i"

class GameManager
{
private:
	GameManager();
	~GameManager();

public:
	static GameManager* getInstance();

	CC_SYNTHESIZE(bool, ActiveSound, ActiveSound);
	CC_SYNTHESIZE(bool, ActiveMusic, ActiveMusic);
	CC_SYNTHESIZE(string, Language, Language);
	CC_SYNTHESIZE(int, CurrentLevel, CurrentLevel);
	CC_SYNTHESIZE(int, GameOverStars, GameOverStars);
	CC_SYNTHESIZE(long, GameOverScore, GameOverScore);
};

void vibrate(int milliseconds);

#endif
