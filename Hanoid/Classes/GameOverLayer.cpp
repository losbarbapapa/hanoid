#include "GameOverLayer.h"
#include "MenuLayer.h"
#include "MainLayer.h"
#include "GameManager.h"
#include "LanguageManager.h"

USING_NS_CC;

Scene* GameOverLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameOverLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameOverLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 128)))
    {
        return false;
    }

    int stars = GameManager::getInstance()->getGameOverStars();
    long myScore = GameManager::getInstance()->getGameOverScore();

	cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	cocos2d::Size halfVisibleSize = visibleSize;
    halfVisibleSize.width /= 2;
    halfVisibleSize.height /= 2;

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("dialogs-hd.plist");
    
	// Background
    auto bkg = Sprite::createWithSpriteFrameName("cloud");
    bkg->setPosition(Vec2(halfVisibleSize.width, halfVisibleSize.height));
    this->addChild(bkg);
	
    // Result Text
	auto resultText = stars == 1 ? LanguageManager::getInstance()->getStringForKey("acceptableText")
			: (stars == 2 ? LanguageManager::getInstance()->getStringForKey("excellentText") : "Super!");

	Label *labelResult = Label::createWithTTF(resultText, FORTE_FONT, 120);
	labelResult->setPosition(halfVisibleSize.width + 2, halfVisibleSize.height + 200 - 2);
	labelResult->setColor(Color3B::BLACK);
	this->addChild(labelResult, 10);

	labelResult = Label::createWithTTF(resultText, FORTE_FONT, 120);
	labelResult->setPosition(halfVisibleSize.width - 2, halfVisibleSize.height + 200 + 2);
	labelResult->setColor(Color3B::BLACK);
	this->addChild(labelResult, 10);

	labelResult = Label::createWithTTF(resultText, FORTE_FONT, 120);
	labelResult->setPosition(halfVisibleSize.width, halfVisibleSize.height + 200);
	labelResult->setColor(Color3B(0, 202, 0));
	this->addChild(labelResult, 10);

	// Stars
	Sprite *starsSprite = Sprite::createWithSpriteFrameName(StringUtils::format("stars%i", stars));
	starsSprite->setPosition(halfVisibleSize.width, halfVisibleSize.height + 100);
	this->addChild(starsSprite);

	// Score
	Label *scoreTitle = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("yourScore"), FORTE_FONT, 48);
	scoreTitle->setColor(Color3B(0, 187, 237));
	scoreTitle->setPosition(halfVisibleSize.width, halfVisibleSize.height - 20);
	this->addChild(scoreTitle);

	Label *scoreText = Label::createWithTTF(StringUtils::format("%ld", myScore), FORTE_FONT, 54);
	scoreText->setColor(Color3B(0, 187, 237));
	scoreText->setPosition(halfVisibleSize.width, halfVisibleSize.height - 80);
	this->addChild(scoreText);

	// Select level
	Sprite *levelsSprite = Sprite::createWithSpriteFrameName("level_select");
	Sprite *levelsOverSprite = Sprite::createWithSpriteFrameName("level_select_over");

	// Reset level
	Sprite *resetSprite = Sprite::createWithSpriteFrameName("reset_level");
	Sprite *resetOverSprite = Sprite::createWithSpriteFrameName("reset_level_over");

	// Next button
	Sprite *nextSprite = Sprite::createWithSpriteFrameName(StringUtils::format("next_%s", GameManager::getInstance()->getLanguage().c_str()));
	Sprite *nextOverSprite = Sprite::createWithSpriteFrameName(StringUtils::format("next_over_%s", GameManager::getInstance()->getLanguage().c_str()));

	MenuItemSprite *levelsButton = MenuItemSprite::create(levelsSprite, levelsOverSprite, CC_CALLBACK_1(GameOverLayer::levelsCallback, this));
	levelsButton->setPosition(halfVisibleSize.width - 220, halfVisibleSize.height - 120);

	MenuItemSprite *resetButton = MenuItemSprite::create(resetSprite, resetOverSprite, CC_CALLBACK_1(GameOverLayer::resetCallback, this));
	resetButton->setPosition(halfVisibleSize.width + 220, halfVisibleSize.height - 120);

	MenuItemSprite *nextButton = MenuItemSprite::create(nextSprite, nextOverSprite, CC_CALLBACK_1(GameOverLayer::nextCallback, this));
	nextButton->setPosition(halfVisibleSize.width, halfVisibleSize.height - 220);

	Menu *menu = Menu::create(levelsButton, resetButton, nextButton, NULL);
	menu->setPosition(0,0);
	this->addChild(menu);

	return true;
}

void GameOverLayer::levelsCallback(Ref* pSender){
    
    auto levels = CallFuncN::create(this, callfuncN_selector(MainLayer::levelsCallback));
    levels->execute();
    
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("dialogs-hd.plist");
    this->removeFromParentAndCleanup(true);
}

void GameOverLayer::resetCallback(Ref* pSender){
    auto main = (MainLayer*)Director::getInstance()->getRunningScene()->getChildByName("MainLayer");
    main->m_bIsPlayable = true;
    main->m_mMenu->setEnabled(true);
    main->m_speHelp->getActionManager()->resumeTarget(main->m_speHelp);
    
    auto next = CallFuncN::create(this, callfuncN_selector(MainLayer::resetCallback));
    next->execute();
    
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("dialogs-hd.plist");
    this->removeFromParentAndCleanup(true);
}

void GameOverLayer::nextCallback(Ref* pSender){
    auto main = (MainLayer*)Director::getInstance()->getRunningScene()->getChildByName("MainLayer");
    main->m_bIsPlayable = true;
    main->m_mMenu->setEnabled(true);
    main->m_speHelp->getActionManager()->resumeTarget(main->m_speHelp);
    
    auto next = CallFuncN::create(this, callfuncN_selector(MainLayer::nextCallback));
    next->execute();
    
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("dialogs-hd.plist");
    this->removeFromParentAndCleanup(true);
}
