#include "HelpLayer.h"
#include "MenuLayer.h"
#include "MainLayer.h"
#include "GameManager.h"
#include "LanguageManager.h"
#include "SonarFrameworks.h"

USING_NS_CC;

Scene* HelpLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelpLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelpLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 128)))
    {
        return false;
    }
    
    SonarCocosHelper::AdMob::hideBannerAd();

    m_bChecked = UserDefault::getInstance()->getIntegerForKey("showInfo") == 1;

	cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	cocos2d::Size halfVisibleSize = visibleSize;
    halfVisibleSize.width /= 2;
    halfVisibleSize.height /= 2;

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("check-hd.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("help_title-hd.plist");

	// Background
    auto bkg = Sprite::create("help_bkg-hd.png");
    bkg->setPosition(Vec2(halfVisibleSize.width, halfVisibleSize.height));
    this->addChild(bkg);
	
    // Boton cerrar comun
	MenuItemSprite *closeButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("close"), Sprite::createWithSpriteFrameName("close_over"),
			CC_CALLBACK_1(HelpLayer::closeCallback, this));
	closeButton->setPosition(863, 540);

	Menu *menu = Menu::create(closeButton, NULL);
	menu->setPosition(0,0);
	this->addChild(menu);
	//

	// Check comun
	MenuItemSprite *checkButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("check_frame"), Sprite::createWithSpriteFrameName("check_frame"),
			CC_CALLBACK_1(HelpLayer::checkCallback, this));
	checkButton->setPosition(110, 80);

	checkSprite = Sprite::createWithSpriteFrameName("check_pigeon");
	checkSprite->setPosition(114,86);
	checkSprite->setVisible(m_bChecked);
	this->addChild(checkSprite);

	Label *infoLabel = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("showInfo"), FORTE_FONT, 38);
	infoLabel->setColor(Color3B::BLACK);
	infoLabel->setPosition(infoLabel->getContentSize().width / 2 + 140, 76);
	this->addChild(infoLabel);

	menu->addChild(checkButton);
	//

	// Paging comun
	pagingSprite = Sprite::createWithSpriteFrameName("paging");
	pagingSprite->setPosition(800, 100);
	this->addChild(pagingSprite);
	//

	// How play (page 1)
	int tagCount = 1;
	Sprite *howPlayTitleSprite = Sprite::createWithSpriteFrameName(StringUtils::format("how_play_%s", GameManager::getInstance()->getLanguage().c_str()));
	howPlayTitleSprite->setPosition(halfVisibleSize.width, visibleSize.height - 100);
	howPlayTitleSprite->setTag(tagCount);
	this->addChild(howPlayTitleSprite);

	cocos2d::Size containerSize = {780, 120};
	for(int i = 1; i < 5; i++)
	{
		tagCount++;
		Label *infoLabel = Label::createWithSystemFont(LanguageManager::getInstance()->getStringForKey(StringUtils::format("info%i", i)), FORTE_FONT, 36,
				containerSize, i==1?TextHAlignment::CENTER:TextHAlignment::LEFT);
		infoLabel->setPosition(visibleSize.width / 2 + 2, visibleSize.height - (i==1 ? 200 : ((i==2) ? 300 : ((i==3) ? 380 : 500))) - 2);
		infoLabel->setColor(Color3B::BLACK);
		infoLabel->setTag(tagCount);
		this->addChild(infoLabel);

		tagCount++;
        Label *infoLabelShadow = Label::createWithSystemFont(LanguageManager::getInstance()->getStringForKey(StringUtils::format("info%i", i)), FORTE_FONT, 36,
				containerSize, i==1?TextHAlignment::CENTER:TextHAlignment::LEFT);
		infoLabelShadow->setPosition(visibleSize.width / 2, visibleSize.height - (i==1 ? 200 : ((i==2) ? 300 : ((i==3) ? 380 : 500))));
		infoLabelShadow->setColor(Color3B((i!=1)?255:0, 0, (i==1)?128:0));
		infoLabelShadow->setTag(tagCount);
		this->addChild(infoLabelShadow);
	}
	//

	// Guide (page 2)
	Sprite *helpTitleSprite = Sprite::createWithSpriteFrameName(StringUtils::format("help_%s", GameManager::getInstance()->getLanguage().c_str()));
	helpTitleSprite->setPosition(visibleSize.width / 2, visibleSize.height - 100);
	helpTitleSprite->setTag(10);
	helpTitleSprite->setVisible(false);
	this->addChild(helpTitleSprite);

	int iconsX = 140, iconsY = 460, distIcons = 140;

	// PC
	Sprite *pcIcon = Sprite::createWithSpriteFrameName("pc");
	pcIcon->setPosition(iconsX, iconsY);
	pcIcon->setTag(11);
	pcIcon->setVisible(false);
	this->addChild(pcIcon);

	Label *pcInfoLabelShadow = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("pcInfo"), FORTE_FONT, 36);
	pcInfoLabelShadow->setPosition(iconsX + 60 + pcInfoLabelShadow->getContentSize().width / 2 + 2, iconsY - 2);
	pcInfoLabelShadow->setColor(Color3B::BLACK);
	pcInfoLabelShadow->setTag(12);
	pcInfoLabelShadow->setVisible(false);
	this->addChild(pcInfoLabelShadow);

	Label *pcInfoLabel = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("pcInfo"), FORTE_FONT, 36);
	pcInfoLabel->setPosition(iconsX + 60 + pcInfoLabelShadow->getContentSize().width / 2, iconsY);
	pcInfoLabel->setColor(Color3B(0, 0, 128));
	pcInfoLabel->setTag(13);
	pcInfoLabel->setVisible(false);
	this->addChild(pcInfoLabel);
	//

	// RESET
	Sprite *resetIcon = Sprite::createWithSpriteFrameName("reset");
	resetIcon->setPosition(iconsX, iconsY - distIcons*1);
	resetIcon->setTag(14);
	resetIcon->setVisible(false);
	this->addChild(resetIcon);

	Label *resetInfoLabelShadow = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("resetInfo"), FORTE_FONT, 36);
	resetInfoLabelShadow->setPosition(iconsX + 60 + resetInfoLabelShadow->getContentSize().width / 2 + 2, iconsY - distIcons - 2);
	resetInfoLabelShadow->setColor(Color3B::BLACK);
	resetInfoLabelShadow->setTag(15);
	resetInfoLabelShadow->setVisible(false);
	this->addChild(resetInfoLabelShadow);

	Label *resetInfoLabel = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("resetInfo"), FORTE_FONT, 36);
	resetInfoLabel->setPosition(iconsX + 60 + resetInfoLabel->getContentSize().width / 2, iconsY - distIcons);
	resetInfoLabel->setColor(Color3B(0, 0, 128));
	resetInfoLabel->setTag(16);
	resetInfoLabel->setVisible(false);
	this->addChild(resetInfoLabel);
	//

	// PAUSE
	Sprite *pauseIcon = Sprite::createWithSpriteFrameName("pause");
	pauseIcon->setPosition(iconsX, iconsY - distIcons*2);
	pauseIcon->setTag(17);
	pauseIcon->setVisible(false);
	this->addChild(pauseIcon);

	Label *pauseInfoLabelShadow = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("pauseInfo"), FORTE_FONT, 36);
	pauseInfoLabelShadow->setPosition(iconsX + 60 + pauseInfoLabelShadow->getContentSize().width / 2 + 2, iconsY - distIcons*2 - 6);
	pauseInfoLabelShadow->setColor(Color3B::BLACK);
	pauseInfoLabelShadow->setTag(18);
	pauseInfoLabelShadow->setVisible(false);
	this->addChild(pauseInfoLabelShadow);

	Label *pauseInfoLabel = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("pauseInfo"), FORTE_FONT, 36);
	pauseInfoLabel->setPosition(iconsX + 60 + pauseInfoLabelShadow->getContentSize().width / 2, iconsY - distIcons*2 - 4);
	pauseInfoLabel->setColor(Color3B(0, 0, 128));
	pauseInfoLabel->setTag(19);
	pauseInfoLabel->setVisible(false);
	this->addChild(pauseInfoLabel);
	//

	auto listener = cocos2d::EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(HelpLayer::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

bool HelpLayer::onTouchBegan(const cocos2d::Touch *touch, cocos2d::Event *unused_event) {
    auto location = touch->getLocationInView();
    location = cocos2d::Director::getInstance()->convertToGL(location);

    Rect rect = Rect(746, 68, 106, 72);
	CCLOG("%f - %f", location.x, location.y);

	if(rect.containsPoint(location)) {
		if(GameManager::getInstance()->getActiveSound())
			SimpleAudioEngine::getInstance()->playEffect("flip.wav");

		m_bPageOne = !m_bPageOne;
		pagingSprite->setFlippedX(!pagingSprite->isFlippedX());

		for(int i = 1; i <= 19; i++)
			this->getChildByTag(i)->setVisible(i > 9 ? m_bPageOne : !m_bPageOne);
	}

    return  true;
}

void HelpLayer::checkCallback(Ref* pSender){
	m_bChecked = !m_bChecked;
	checkSprite->setVisible(m_bChecked);

	UserDefault::getInstance()->setIntegerForKey("showInfo", m_bChecked ? 1 : 2);
	UserDefault::getInstance()->flush();
}

void HelpLayer::closeCallback(Ref* pSender){
	auto main = (MainLayer*)Director::getInstance()->getRunningScene()->getChildByName("MainLayer");
    main->m_bIsPlayable = true;
	main->m_mMenu->setEnabled(true);
	main->m_speHelp->getActionManager()->resumeTarget(main->m_speHelp);

	auto play = CallFuncN::create(this, callfuncN_selector(MainLayer::playCallback));
	play->execute();

	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("help_title-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("check-hd.plist");

	if(GameManager::getInstance()->getActiveSound())
		SimpleAudioEngine::getInstance()->playEffect("close_button.wav");

	this->removeFromParentAndCleanup(true);

	SonarCocosHelper::AdMob::showBannerAd(SonarCocosHelper::AdBannerPosition::eBottom);
}
