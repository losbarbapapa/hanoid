#ifndef __HELP_LAYER_H__
#define __HELP_LAYER_H__

#include "cocos2d.h"

class HelpLayer : public cocos2d::LayerColor
{
private:
	bool m_bChecked, m_bPageOne;
	cocos2d::Sprite *checkSprite, *pagingSprite;
    
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    virtual bool onTouchBegan(const cocos2d::Touch *touch, cocos2d::Event *unused_event);
    
    // a selector callback
    void checkCallback(Ref* pSender);
	void closeCallback(Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelpLayer);
};

#endif // __HELP_LAYER_H__
