#include "HighscoresLayer.h"
#include "MenuLayer.h"
#include "GameManager.h"
#include "LanguageManager.h"

USING_NS_CC;

Scene* HighscoresLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HighscoresLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HighscoresLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("layerTitleIcons-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("levels-hd.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("buttons-hd.plist");

    // Background
	auto bkg = Sprite::create("top_scores-hd.png");
	bkg->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	this->addChild(bkg, 0);

    auto title = Sprite::createWithSpriteFrameName(StringUtils::format("top_scores_%s", GameManager::getInstance()->getLanguage().c_str()));
    title->setPosition(visibleSize.width/2, visibleSize.height - 60);
    this->addChild(title);
    
	// Back button
	auto margin = 10;
    auto backButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("back"), Sprite::createWithSpriteFrameName("back_over"), CC_CALLBACK_1(HighscoresLayer::backCallback, this));

	auto backBtnSize = backButton->getContentSize();

	auto menuBack = Menu::create(backButton, NULL);
	menuBack->setPosition(Vec2(backBtnSize.width/2 + margin, visibleSize.height - backBtnSize.width/2 - margin));
	this->addChild(menuBack, 2);
	//

    cocos2d::ui::PageView* pageView = cocos2d::ui::PageView::create();
    pageView->setSize(visibleSize);
    
    for (int i = 0; i < 8; i++)
    {
        char scoreLevelTxt[13]; //level#_score#
        sprintf(scoreLevelTxt, LEVEL_SCORE, i+1, 1);
        auto scoreLevel = UserDefault::getInstance()->getIntegerForKey(scoreLevelTxt);
        
        auto layout = cocos2d::ui::Layout::create();
        layout->setSize(visibleSize);
        
        auto txtLevel = StringUtils::format(LanguageManager::getInstance()->getStringForKey("level").c_str(), i+1);
        auto lblLevel = Label::createWithTTF(txtLevel, FORTE_FONT, 60);
        lblLevel->setPosition(visibleSize.width/2, visibleSize.height - 160);
        lblLevel->setColor(Color3B::BLACK);
        layout->addChild(lblLevel);
        
        if(scoreLevel == 0)
        {
            auto txtNoScore = StringUtils::format(LanguageManager::getInstance()->getStringForKey("noScore").c_str(), i+1);
            auto lblNoScore = Label::createWithTTF(txtNoScore, FORTE_FONT, 60);
            lblNoScore->setPosition(visibleSize.width/2, visibleSize.height/2);
            lblNoScore->setColor(Color3B::BLACK);
            layout->addChild(lblNoScore);
        }
        else
        {
            for(int j = 1; j <= 5; j++)
            {
                auto scoreLevel = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(LEVEL_SCORE, i+1, j).c_str());
                auto starsLevel = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(LEVEL_STARS, i+1, j).c_str());
                
                if(scoreLevel > 0)
                {
                    auto txtLevelNo = StringUtils::format("%i.", j);
                    auto lblLevelNo = Label::createWithTTF(txtLevelNo, FORTE_FONT, 45);
                    lblLevelNo->setPosition(visibleSize.width/2 - 230, visibleSize.height - 240 - (j-1)*55);
                    lblLevelNo->setColor(Color3B::BLACK);
                    layout->addChild(lblLevelNo);
                    
                    auto spLevelStars = Sprite::createWithSpriteFrameName(StringUtils::format("levelStars%i", starsLevel));
                    spLevelStars->setPosition(visibleSize.width/2 - 120, visibleSize.height - 230 - (j-1)*55);
                    layout->cocos2d::Node::addChild(spLevelStars);
                    
                    auto scoreText = StringUtils::format(LanguageManager::getInstance()->getStringForKey("score").c_str(), scoreLevel);
                    auto lblscoreText = Label::createWithTTF(scoreText, FORTE_FONT, 45);
                    lblscoreText->setPosition(visibleSize.width/2 + 100, visibleSize.height - 240 - (j-1)*55);
                    lblscoreText->setColor(Color3B::BLACK);
                    layout->addChild(lblscoreText);
                }
            }
        }
        
        pageView->addPage(layout);
    }
    
    this->addChild(pageView);
    
    auto keyboardListener = cocos2d::EventListenerKeyboard::create();
    keyboardListener->onKeyReleased = CC_CALLBACK_2(HighscoresLayer::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);
    
    auto marginLevelInd = 10;
    auto currentLevel = Sprite::create("currentLevel.png");
    auto unselectedLevel = Sprite::create("unselectedLevel.png");
    
    auto width = currentLevel->getContentSize().width + (unselectedLevel->getContentSize().width + marginLevelInd) * 7;
    auto X = (visibleSize.width - width) / 2;
    auto Y = 110;
    
    for(int i = 1; i < 9; i++)
    {
        if(i == 1){
            currentLevel->setPosition(Vec2(X, Y));
            currentLevel->setName("currentLevel");
            this->addChild(currentLevel);
            X += currentLevel->getContentSize().width + marginLevelInd;
        }else{
            auto unselLevel = Sprite::create("unselectedLevel.png");
            unselLevel->setPosition(Vec2(X, Y));
            unselLevel->setName(StringUtils::format("level_%i", i));
            this->addChild(unselLevel);
            X += currentLevel->getContentSize().width + marginLevelInd;
        }
    }
    
    pageView->addEventListener(CC_CALLBACK_2(HighscoresLayer::pageViewEvent, this));

    return true;
}

void HighscoresLayer::pageViewEvent(Ref *pSender, cocos2d::ui::PageView::EventType type)
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    auto marginLevelInd = 10;
    auto currentLevel = Sprite::create("currentLevel.png");
    auto unselectedLevel = Sprite::create("unselectedLevel.png");
    
    auto width = currentLevel->getContentSize().width + (unselectedLevel->getContentSize().width + marginLevelInd) * 7;
    auto X = (visibleSize.width - width) / 2;
    auto Y = 110;
    
    auto children = this->getChildren();
    
    switch (type)
    {
        case cocos2d::ui::PageView::EventType::TURNING:
        {
            cocos2d::ui::PageView* pageView = dynamic_cast<cocos2d::ui::PageView*>(pSender);
            auto iCurrLevel = pageView->getCurPageIndex() + 1;
            long prevSel = iCurrLevel;
            
            for(int i = 1; i < 9; i++)
            {
                if(i == iCurrLevel){
                    this->getChildByName("currentLevel")->setPosition(Vec2(X, Y));
                    X += currentLevel->getContentSize().width + marginLevelInd;
                }else{
                    auto unselLevel = this->getChildByName(StringUtils::format("level_%i", i));
                    if(unselLevel == nullptr){
                        auto prevS = StringUtils::format("level_%ld", prevSel);
                        unselLevel = this->getChildByName(StringUtils::format("level_%ld", prevSel));
                    }
                    
                    unselLevel->setPosition(Vec2(X, Y));
                    currentLevel->setName(StringUtils::format("level_%i", i));
                    
                    X += this->getChildByName("currentLevel")->getContentSize().width + marginLevelInd;
                }
            }
        }
            break;
            
        default:
            break;
    }
}

void HighscoresLayer::backCallback(Ref* pSender)
{
	if(GameManager::getInstance()->getActiveSound()){
		SimpleAudioEngine::getInstance()->playEffect("button.wav");
	}

	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("layerTitleIcons-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("levels-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("buttons-hd.plist");

	Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, MenuLayer::createScene()));
}

void HighscoresLayer::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
	{
		this->backCallback(NULL);
	}
}
