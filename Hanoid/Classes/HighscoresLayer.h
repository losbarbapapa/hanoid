#ifndef __HIGHSCORES_LAYER_H__
#define __HIGHSCORES_LAYER_H__

#include "cocos2d.h"
#include "ui/UIPageView.h"

class HighscoresLayer : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void pageViewEvent(Ref *pSender, cocos2d::ui::PageView::EventType type);
    void backCallback(Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HighscoresLayer);

    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
};

#endif // __HIGHSCORES_LAYER_H__
