#include "LanguageManager.h"

#include "cocos2d.h"
using namespace cocos2d;

LanguageManager* LanguageManager::_instance = 0;

LanguageManager::LanguageManager()
{
	string language = UserDefault::getInstance()->getStringForKey("language");
    string fileName;


    if(strcmp(language.c_str(), "") == 0){
    	string appLang = Application::getInstance()->getCurrentLanguageCode();
		if(strcmp(appLang.c_str(), "en") == 0){
			fileName = "en.json"; // english language
		}else if(strcmp(appLang.c_str(), "es") == 0){
			fileName = "es.json"; // spanish language
		}else{
			fileName = "en.json"; // default
		}
    }else{
    	fileName = language + ".json";
    }

    string content = FileUtils::getInstance()->getStringFromFile(fileName.c_str());
    string clearContent = content.substr(0, content.rfind('}') + 1);

    _document.Parse<0>(clearContent.c_str());
    if(_document.HasParseError())
    {
        CCLOG("Language file parsing errors");
        return;
    }
}

LanguageManager* LanguageManager::getInstance()
{
    if(!_instance)
        _instance = new LanguageManager();
    return _instance;
}

string LanguageManager::getStringForKey(string key) const
{
    return _document[key.c_str()].GetString();
}

void LanguageManager::setLanguage(string newLang)
{
	string fileName = newLang + ".json";

	ssize_t size;
	const char* buf = (const char*)FileUtils::getInstance()->getFileData(fileName.c_str(), "r", &size);
	string content(buf);
	string clearContent = content.substr(0, content.rfind('}') + 1);

	_document.Parse<0>(clearContent.c_str());
	if(_document.HasParseError())
	{
		CCLOG("Language file parsing errors");
		return;
	}
}
