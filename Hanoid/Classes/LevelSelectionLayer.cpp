#include "LevelSelectionLayer.h"
#include "MenuLayer.h"
#include "GameManager.h"
#include "MainLayer.h"
#include "ui/UIButton.h"
#include "ui/UILayout.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include <AudioToolbox/AudioToolbox.h>
#endif

USING_NS_CC;

Scene* LevelSelectionLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = LevelSelectionLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool LevelSelectionLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
	cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	cocos2d::Size halfVisibleSize = visibleSize;
    halfVisibleSize.width /= 2;
    halfVisibleSize.height /= 2;

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("buttons-hd.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("levels-hd.plist");

	// Background
    auto bkg = Sprite::create("levels_bkg-hd.png");
    bkg->setPosition(Vec2(halfVisibleSize.width, halfVisibleSize.height));
    this->addChild(bkg, 0);
	
			
	auto margin = 10;

	// Back button
	auto backButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("back"), Sprite::createWithSpriteFrameName("back_over"), CC_CALLBACK_1(LevelSelectionLayer::backCallback, this));
	
	auto backBtnSize = backButton->getContentSize();

    auto menuBack = Menu::create(backButton, NULL);
    menuBack->setPosition(Vec2(backBtnSize.width/2 + margin, visibleSize.height - backBtnSize.width/2 - margin));
	this->addChild(menuBack, 2);
	//

	auto cloudSpriteSize = Sprite::createWithSpriteFrameName("level1")->getContentSize();
	auto cloudMargin = halfVisibleSize.width - cloudSpriteSize.width*1.25;
	auto innerSize = visibleSize;
	innerSize.width = (cloudSpriteSize.width + cloudMargin) * 12 - cloudSpriteSize.width;

	cocos2d::ui::PageView* pageView = cocos2d::ui::PageView::create();
	pageView->setSize(visibleSize);

	auto lastLevel = 1;

	for (int i = 0; i < 8; i++)
	{
		auto spriteName = StringUtils::format("level%i.png", i+1);

		char scoreLevelTxt[13]; //level#_score#
		sprintf(scoreLevelTxt, LEVEL_SCORE, i+1, 1);
		auto scoreLevel = UserDefault::getInstance()->getIntegerForKey(scoreLevelTxt);

		char starsLevelTxt[13]; //level#_stars#
		sprintf(starsLevelTxt, LEVEL_STARS, i+1, 1);
		auto starsLevel = UserDefault::getInstance()->getIntegerForKey(starsLevelTxt);

		auto layout = cocos2d::ui::Layout::create();
		layout->setSize(visibleSize);

		if(scoreLevel == 0 && i > 0 && UserDefault::getInstance()->getIntegerForKey(StringUtils::format(LEVEL_SCORE,i,1).c_str()) == 0)
		{
            spriteName = "levelLocked.png";
		}
		else
		{
			char levelStars[11];
			sprintf(levelStars, "levelStars%i", starsLevel);
			auto starSprite = Sprite::createWithSpriteFrameName(levelStars);
			starSprite->setPosition(Vec2(halfVisibleSize.width, halfVisibleSize.height - 100));
			layout->addChild(starSprite, 1);
			lastLevel = i + 1;
		}

		cocos2d::ui::Button *btn = cocos2d::ui::Button::create(spriteName,spriteName);
		btn->setPosition(halfVisibleSize);
		btn->setTag(i+1);
		btn->addTouchEventListener(CC_CALLBACK_2(LevelSelectionLayer::openLevel, this));
        layout->addChild(btn);

		pageView->addPage(layout);//insertPage(outerBox,i);
	}

	this->addChild(pageView);

	auto keyboardListener = cocos2d::EventListenerKeyboard::create();
	keyboardListener->onKeyReleased = CC_CALLBACK_2(LevelSelectionLayer::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	auto marginLevelInd = 10;
	auto currentLevel = Sprite::create("currentLevel.png");
	auto unselectedLevel = Sprite::create("unselectedLevel.png");

	auto width = currentLevel->getContentSize().width + (unselectedLevel->getContentSize().width + marginLevelInd) * 7;
	auto X = (visibleSize.width - width) / 2;
	auto Y = 110;

	for(int i = 1; i < 9; i++)
	{
		if(i == lastLevel){
			currentLevel->setPosition(Vec2(X, Y));
			currentLevel->setName("currentLevel");
			this->addChild(currentLevel);
			X += currentLevel->getContentSize().width + marginLevelInd;
		}else{
			auto unselLevel = Sprite::create("unselectedLevel.png");
			unselLevel->setPosition(Vec2(X, Y));
			unselLevel->setName(StringUtils::format("level_%i", i));
			this->addChild(unselLevel);
			X += currentLevel->getContentSize().width + marginLevelInd;
		}
	}

	pageView->addEventListener(CC_CALLBACK_2(LevelSelectionLayer::pageViewEvent, this));
    pageView->scrollToPage(lastLevel-1);
    
	return true;
}

void LevelSelectionLayer::pageViewEvent(Ref *pSender, cocos2d::ui::PageView::EventType type)
{
	cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
	auto marginLevelInd = 10;
	auto currentLevel = Sprite::create("currentLevel.png");
	auto unselectedLevel = Sprite::create("unselectedLevel.png");

	auto width = currentLevel->getContentSize().width + (unselectedLevel->getContentSize().width + marginLevelInd) * 7;
	auto X = (visibleSize.width - width) / 2;
	auto Y = 110;
	
	auto children = this->getChildren();
	
	switch (type)
    {
        case cocos2d::ui::PageView::EventType::TURNING:
        {
        	cocos2d::ui::PageView* pageView = dynamic_cast<cocos2d::ui::PageView*>(pSender);
        	auto iCurrLevel = pageView->getCurPageIndex() + 1;
        	long prevSel = iCurrLevel;

        	for(int i = 1; i < 9; i++)
			{
				if(i == iCurrLevel){
					this->getChildByName("currentLevel")->setPosition(Vec2(X, Y));
					X += currentLevel->getContentSize().width + marginLevelInd;
				}else{
					auto unselLevel = this->getChildByName(StringUtils::format("level_%i", i));
					if(unselLevel == nullptr){
						auto prevS = StringUtils::format("level_%ld", prevSel);
						unselLevel = this->getChildByName(StringUtils::format("level_%ld", prevSel));
					}

					unselLevel->setPosition(Vec2(X, Y));
					currentLevel->setName(StringUtils::format("level_%i", i));

					X += this->getChildByName("currentLevel")->getContentSize().width + marginLevelInd;
				}
			}
        }
            break;

        default:
            break;
    }
}

void LevelSelectionLayer::openLevel(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
	auto cloudItem = (cocos2d::ui::Button*)pSender;
	auto level = cloudItem->getTag();
	CCLOG("Nivel seleccionado: %i", level);

	bool bLevelEnabled = true;

	if(level != 1)
	{
		char scoreLevelTxt[13]; //level#_score#
		sprintf(scoreLevelTxt, LEVEL_SCORE, level, 1);
		auto scoreLevel = UserDefault::getInstance()->getIntegerForKey(scoreLevelTxt);
		if(scoreLevel == 0 && UserDefault::getInstance()->getIntegerForKey(StringUtils::format(LEVEL_SCORE,level-1,1).c_str()) == 0)
			bLevelEnabled = false;
	}

	if(bLevelEnabled)
	{
		if(GameManager::getInstance()->getActiveSound())
		{
			SimpleAudioEngine::getInstance()->playEffect("cloud_button.wav");
		}

		GameManager::getInstance()->setCurrentLevel(level);

		SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("levels-hd.plist");
		SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("buttons-hd.plist");

		Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, MainLayer::createScene()));
        return;
	}
	else
	{
		if(GameManager::getInstance()->getActiveSound())
		{
			SimpleAudioEngine::getInstance()->playEffect("error.wav");
		}
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		vibrate(200);
#endif
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
#endif
	}
}

void LevelSelectionLayer::backCallback(Ref* pSender)
{
	if(GameManager::getInstance()->getActiveSound()){
		SimpleAudioEngine::getInstance()->playEffect("button.wav");
	}
	
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("levels-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("buttons-hd.plist");
	
	Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, MenuLayer::createScene()));
}

void LevelSelectionLayer::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
	{
		this->backCallback(NULL);
	}
}
