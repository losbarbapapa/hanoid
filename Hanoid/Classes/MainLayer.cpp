#include "MainLayer.h"
#include "LevelSelectionLayer.h"
#include "PauseLayer.h"
#include "HelpLayer.h"
#include "MenuLayer.h"
#include "GameOverLayer.h"
#include "GameManager.h"
#include "LanguageManager.h"

USING_NS_CC;

Scene* MainLayer::createScene() {
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainLayer::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainLayer::init() {
	//////////////////////////////
	// 1. super init first
	if (!Layer::init()) {
		return false;
	}
    
    this->setName("MainLayer");
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	cocos2d::Size halfVisibleSize = visibleSize;
	halfVisibleSize.width /= 2;
	halfVisibleSize.height /= 2;

    // Valores iniciales indicando que se puede jugar
    m_iStackSelected = 0;
    m_bIsPlayable = true;
    m_bPlaysPC = false;
    m_iMoveCounter = 0;
    m_timeState = stopped;
    m_iCountDown = 3;
    
    // Background...
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("bg_game-hd.plist");
    Sprite *bg = Sprite::createWithSpriteFrameName("bg");
    bg->setPosition(halfVisibleSize);
    this->addChild(bg);
    
    //Buttons
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("buttons-hd.plist");
    
    // Carga sprite sheet de discos...
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("discs_simple-hd.plist");
    
    // Carga sprite sheet de elementos...
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("scene_elements-hd.plist");
    
    // Preparo imagenes del sol y el az de luz...
    m_speSunHeart = Sprite::createWithSpriteFrameName("sun_heart");
    m_speSunHeart->retain();
    
    m_speSunRays = Sprite::createWithSpriteFrameName("sun_rays");
    m_speSunRays->retain();
    
    m_speAz = Sprite::createWithSpriteFrameName("az");
    m_speAz->retain();
    
    // Obtengo nivel actual para determinar numero de discos y cantidad de jugadas minimas...
    m_iNumDiscs = GameManager::getInstance()->getCurrentLevel() + 2;
    m_iNumPlays = pow(2, m_iNumDiscs) - 1; // cantidad mÌnima para ganar el nivel...

    // Preparo marcos que contandran los touches...
    int altura = (BARS_Y - DISCS_HEIGHT / 2) + (m_iNumDiscs)*DISCS_HEIGHT;
    float diffDiscs = 10;
    float middleTopDisc = 45;
    m_ptFrame1.push_back(Vec2(BAR1_X-diffDiscs*(m_iNumDiscs-1)-middleTopDisc, BARS_Y - DISCS_HEIGHT / 2));
    m_ptFrame1.push_back(Vec2(BAR1_X+diffDiscs*(m_iNumDiscs-1)+middleTopDisc, BARS_Y - DISCS_HEIGHT / 2));
    m_ptFrame1.push_back(Vec2(BAR1_X+middleTopDisc, altura));
    m_ptFrame1.push_back(Vec2(BAR1_X-middleTopDisc, altura));
    
    m_ptFrame2.push_back(Vec2(BAR2_X-diffDiscs*(m_iNumDiscs-1)-middleTopDisc, BARS_Y - DISCS_HEIGHT / 2));
    m_ptFrame2.push_back(Vec2(BAR2_X+diffDiscs*(m_iNumDiscs-1)+middleTopDisc, BARS_Y - DISCS_HEIGHT / 2));
    m_ptFrame2.push_back(Vec2(BAR2_X+middleTopDisc, altura));
    m_ptFrame2.push_back(Vec2(BAR2_X-middleTopDisc, altura));
    
    m_ptFrame3.push_back(Vec2(BAR3_X-diffDiscs*(m_iNumDiscs-1)-middleTopDisc, BARS_Y - DISCS_HEIGHT / 2));
    m_ptFrame3.push_back(Vec2(BAR3_X+diffDiscs*(m_iNumDiscs-1)+middleTopDisc, BARS_Y - DISCS_HEIGHT / 2));
    m_ptFrame3.push_back(Vec2(BAR3_X+middleTopDisc, altura));
    m_ptFrame3.push_back(Vec2(BAR3_X-middleTopDisc, altura));
    
    // Creo arreglos y pilas
    m_skStack1.name = "stack1";
    m_skStack2.name = "stack2";
    m_skStack3.name = "stack3";
    
    // Creo barra 1 de discos...
    for(int i = m_iNumDiscs, j = 0; i >= 1; i--, j++) {
        auto sp = DiscSprite::create();
        sp->setSpriteFrame(StringUtils::format("%d.png", i));
        sp->setPosition(BAR1_X, BARS_Y + (DISCS_HEIGHT + DISCS_SEP) * j);
        sp->myPos = sp->getPosition();
        this->addChild(sp, zDiscs);
        m_skStack1.items.push(sp);
    }
    
    m_maStacks.push_back(m_skStack1);
    
    // Si la cantidad de discos es par se empieza moviendo a la barra 2...
    if(m_iNumDiscs%2 == 0){
        m_maStacks.push_back(m_skStack3);
        m_maStacks.push_back(m_skStack2);
    } else {
        m_maStacks.push_back(m_skStack2);
        m_maStacks.push_back(m_skStack3);
    }
    
    // Botones del menu
    MenuItemSprite *pcButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("pc"),
                                                      Sprite::createWithSpriteFrameName("pc_over"),
                                                      Sprite::createWithSpriteFrameName("pc_disabled"),
                                                      CC_CALLBACK_1(MainLayer::MovePC, this));
    
    MenuItemSprite *resetButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("reset"),
                                                      Sprite::createWithSpriteFrameName("reset_over"),
                                                      Sprite::createWithSpriteFrameName("reset_disabled"),
                                                      CC_CALLBACK_1(MainLayer::resetLevel, this));
    resetButton->setPosition(780, 0);
    
    MenuItemSprite *pauseButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("pause"),
                                                         Sprite::createWithSpriteFrameName("pause_over"),
                                                         Sprite::createWithSpriteFrameName("pause_disabled"),
                                                         CC_CALLBACK_1(MainLayer::pauseGame, this));
    pauseButton->setPosition(880, 0);
    
    m_mMenu = Menu::create(pcButton, resetButton, pauseButton, NULL);
    //m_mMenu->retain();
    m_mMenu->setName("menu");
    m_mMenu->setPosition(40,600);
    this->addChild(m_mMenu, zMenu);
    
    // Pongo bg, cilindro y ground...
    Sprite *ground = Sprite::create("ground-hd.png");
    ground->setPosition(halfVisibleSize.width, 38);
    this->addChild(ground, zGround);
    
    int li_CylY = halfVisibleSize.height - (340 - (m_iNumDiscs - 2)*DISCS_HEIGHT);
    
    Sprite *cylinder1 = Sprite::createWithSpriteFrameName("cylinder");
    cylinder1->setPosition(BAR1_X, li_CylY);
    this->addChild(cylinder1, zCylinders);
    
    Sprite *cylinder2 = Sprite::createWithSpriteFrameName("cylinder");
    cylinder2->setPosition(BAR2_X, li_CylY);
    this->addChild(cylinder2, zCylinders);
    
    Sprite *cylinder3 = Sprite::createWithSpriteFrameName("green_cylinder");
    cylinder3->setPosition(BAR3_X, li_CylY);
    this->addChild(cylinder3, zCylinders);
    
    m_iActionY = (cylinder1->getPosition().y + cylinder1->getContentSize().height / 2) + 12;
    
    // Jugadas
    m_lbmfMovesMade = Label::createWithTTF(StringUtils::format(LanguageManager::getInstance()->getStringForKey("countStop").c_str(),m_iNumPlays), FORTE_FONT, 36);
    m_lbmfMovesMade->setPosition(m_lbmfMovesMade->getContentSize().width/2 + 140, 594);
    m_lbmfMovesMade->setColor(Color3B::BLACK);
    this->addChild(m_lbmfMovesMade, zLabel);
    //
    
    // Preparo reloj...
    Sprite *clockBorder = Sprite::createWithSpriteFrameName("clock_border");
    clockBorder->setPosition(500, 600);
    this->addChild(clockBorder, zMenu);
    
    m_speClockSec = Sprite::createWithSpriteFrameName("clock_sec");
    m_speClockSec->retain();
    m_speClockSec->setPosition(500, 600);
    this->addChild(m_speClockSec, zMenu);
    
    m_iSeconds = 0;
    m_lbmfClock = Label::createWithTTF("00:00:00", FORTE_FONT, 46);
    m_lbmfClock->setPosition(640, 594);
    m_lbmfClock->setColor(Color3B::BLACK);
    this->addChild(m_lbmfClock, zLabel);
    //
    
    // Lanzo creador de nubes...
    this->schedule(CC_SCHEDULE_SELECTOR(MainLayer::createCloud), 5);
    
    // Boton de ayuda...
    m_speHelp = Sprite::create("help-hd.png");
    m_speHelp->setName("helpButton");
    m_speHelp->setPosition(900,20);
    this->addChild(m_speHelp, zGameOver);
    
    auto actRot1 = RotateBy::create(1.0, 15.0);
    auto actRot2 = RotateBy::create(1.0, -15.0);
    auto actSeq1 = Sequence::create(actRot1, actRot2, NULL);
    
    auto actFade1 = FadeTo::create(2.0, 200);
    auto actFade2 = FadeTo::create(2.0, 255);
    auto actSeq2 = Sequence::create(actFade1, actFade2, NULL);
    
    auto actScale1 = ScaleTo::create(1.0, 1.5);
    auto actScale2 = ScaleTo::create(1.0, 1.0);
    auto actSeq3 = Sequence::create(actScale1, actScale2, NULL);
    
    auto actionSpawn = Spawn::create(actSeq1, actSeq2, actSeq3, NULL);
    auto actionRepeat = RepeatForever::create(actionSpawn);
    m_speHelp->runAction(actionRepeat);
    
    // Verifico si debo mostrar la ayuda...
    if(UserDefault::getInstance()->getIntegerForKey("showInfo") == 1){
    	this->showHelp();
    }
    
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(MainLayer::onTouchBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
	return true;
}

bool MainLayer::onTouchBegan(const cocos2d::Touch *touch, cocos2d::Event *unused_event) {
    auto location = touch->getLocationInView();
    location = cocos2d::Director::getInstance()->convertToGL(location);
    this->tapDown(location);
    return  true;
}

void MainLayer::tapDown(cocos2d::Vec2 location)
{
    if(m_bIsPlayable)
    {
        if(this->PointInPolygon(location, m_ptFrame1))
        {
            this->MoveManuallyToFrame(1);
        }
        else if(this->PointInPolygon(location, m_ptFrame2))
        {
            this->MoveManuallyToFrame(2);
        }
        else if(this->PointInPolygon(location, m_ptFrame3))
        {
            this->MoveManuallyToFrame(3);
        }
        else
        {
            this->removeChildByTag(zSunRays, true);
            this->removeChildByTag(zSunHeart, true);
            this->removeChildByTag(zAz, true);
            m_iStackSelected = 0;
        }
        
        Rect rect = Rect(m_speHelp->getPosition().x-22, m_speHelp->getPosition().y - 10, 60, 80);
        CCLOG("ccTouchesBegan  (%f - %f)", location.x, location.y);
        
        if(rect.containsPoint(location)){
        	this->showHelp();
        }
    }
}

void MainLayer::MoveManuallyToFrame(int frameNum){
    Size visibleSize = Director::getInstance()->getVisibleSize();
    cocos2d::Size halfVisibleSize = visibleSize;
    halfVisibleSize.width /= 2;
    halfVisibleSize.height /= 2;
    
    if (m_timeState == stopped) {
        m_bPlaysPC = false;
        m_mMenu->setEnabled(false);
        m_bIsPlayable = false;
        //self.isTouchEnabled = false;
        if(m_maStacks.at(0).items.size() != m_iNumDiscs)
            this->resetLevel(NULL);

        Sprite *countDownNumber = Sprite::createWithSpriteFrameName(StringUtils::format("countDown%i", m_iCountDown));
        countDownNumber->setPosition(halfVisibleSize.width, 640 + 248);
        this->addChild(countDownNumber, zLabel);
        
        auto actMove = MoveTo::create(0.8, halfVisibleSize);
        auto actDelay = DelayTime::create(0.3);
        auto actMove2 = MoveTo::create(0.2, Vec2(halfVisibleSize.width, -248));
        auto actRemove = CallFuncN::create(CC_CALLBACK_1(MainLayer::countDownFinished, this));
        
        countDownNumber->runAction(Sequence::create(actMove, actDelay, actMove2, actRemove, NULL));
        
        if(GameManager::getInstance()->getActiveSound())
            SimpleAudioEngine::getInstance()->playEffect("counter.wav");
    } else {
        
        int origen = -1, destino = -1;
        int X = 0;
        switch (frameNum) {
            case 1:
                origen = (m_iStackSelected == 2) ? 1 : ((m_iStackSelected == 3) ? 2 : -1);
                destino = 0;
                X = BAR1_X;
                break;
            case 2:
                origen = (m_iStackSelected == 1) ? 0 : ((m_iStackSelected == 3) ? 2 : -1);
                destino = 1;
                X = BAR2_X;
                break;
            case 3:
                origen = (m_iStackSelected == 1) ? 0 : ((m_iStackSelected == 2) ? 1 : -1);
                destino = 2;
                X = BAR3_X;
                break;
            default:
                break;
        }
        
        if(m_iNumDiscs%2 == 0){
        	if(origen == 1) origen = 2;
        	else if(origen == 2) origen = 1;

        	if(destino == 1) destino = 2;
        	else if(destino == 2) destino = 1;
        }

        if (m_iStackSelected == 0) {
            if(this->getChildByTag(zSunHeart) == NULL && m_maStacks.at(destino).items.size() > 0){
            //if([self getChildByTag:zSunHeart]==nil && destino.count > 0) {
                m_speSunHeart->setPosition(X, SUN_Y);
                m_speSunRays->setPosition(X, SUN_Y);
                m_speAz->setPosition(X, AZ_Y);
                
                auto actRot1 = RotateBy::create(2.0, 360.0);
                auto actRot2 = actRot1->reverse();
                auto actSeq1 = Sequence::create(actRot1, actRot2, NULL);
                
                auto actFade1 = FadeTo::create(2.0, 150);
                auto actFade2 = FadeTo::create(2.0, 255);
                auto actSeq2 = Sequence::create(actFade1, actFade2, NULL);
                
                auto actionSpawn = Spawn::create(actSeq1, actSeq2, NULL);
                auto actionRepeat = RepeatForever::create(actionSpawn);
                m_speSunRays->runAction(actionRepeat);
                
                this->addChild(m_speSunRays, zSunRays, zSunRays);
                this->addChild(m_speSunHeart, zSunHeart, zSunHeart);
                this->addChild(m_speAz, zAz, zAz);
                m_iStackSelected = frameNum;
            }
        } else {
            this->removeChildByTag(zSunRays, true);
            this->removeChildByTag(zSunHeart, true);
            this->removeChildByTag(zAz, true);
            if(origen != -1 && destino != -1)
                this->MoveDisc(origen, destino, false);
            m_iStackSelected = 0;
        }
    }
}

void MainLayer::tick(float dt) {
	auto actRotate = RotateBy::create(0, 90);
	m_speClockSec->runAction(actRotate);

	m_iSeconds += dt;

	int hours   = m_iSeconds / 3600,
		minutes = m_iSeconds / 60,
		seconds = m_iSeconds % 60;

	auto sClock = StringUtils::format("%02i:%02i:%02i", hours, minutes, seconds);
	m_lbmfClock->setString(sClock);
}

void MainLayer::createCloud(float dt) {
	int randomVel = rand()%4 + 8;
	int cloudId = rand()%3 + 1;
	int randomY = rand()%352 + 228; // Comenzando en y = 114 coger un rango de 176...
	CCLOG("Vel[%i] cloudID[%i] Y[%i]", randomVel, cloudId, randomY);
	auto cloud = Sprite::createWithSpriteFrameName(StringUtils::format("cloud%i",cloudId));
	cloud->setPosition(-200, randomY);
	this->addChild(cloud, zClouds);

	auto actMove = MoveTo::create(randomVel, Vec2(1200, randomY));
	auto actRemove = CallFunc::create(CC_CALLBACK_0(MainLayer::cloudMoveFinished, this, cloud));

	cloud->runAction(Sequence::create(actMove, actRemove, NULL));
}

void MainLayer::cloudMoveFinished(Ref* pSender){
	this->removeChild((Sprite*)pSender, true);
}

bool MainLayer::PointInPolygon(Point point, vector<Point> points) {
    unsigned long i, j, nvert = points.size();
	bool c = false;

	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((points[i].y >= point.y) != (points[j].y >= point.y))
				&& (point.x
						<= (points[j].x - points[i].x) * (point.y - points[i].y)
								/ (points[j].y - points[i].y) + points[i].x))
			c = !c;
	}

	return c;
}

void MainLayer::checkIfWon(cocos2d::Ref *pSender, string stackName, int stackSize){
    m_iMoveCounter++;
    
    //if([m_sLastStack isEqualToString:@"stack3"] && m_iLastStackCount == m_iNumDiscs)
    if(stackName.compare("stack3") == 0 && stackSize == m_iNumDiscs - 1)
    {
        this->unschedule(CC_SCHEDULE_SELECTOR(MainLayer::tick));
        m_timeState = stopped;
        
        m_bIsPlayable = false;
        m_mMenu->setEnabled(false);
        
        m_speHelp->getActionManager()->pauseTarget(m_speHelp);
        this->getActionManager()->pauseTarget(this);
        
        int diff = m_iMoveCounter - m_iNumPlays;
        int stars = (diff == 0) ? 3 : ((diff <= 5) ? 2 : 1);
        
        long myScore = (stars * 1000 + m_iNumPlays * 100) - (m_iMoveCounter * m_iSeconds * 2);
        if(myScore < stars * 1000) myScore = stars * 1000;
        
        //int tempStars = stars;
        //long tempScore = myScore;
        
        CCLOG("STARS: [%i] - SCORE [%ld]", stars, myScore);
        
        // Guardo estrellas y score...
        //int highestScore = 0;
        int i = 1;
        for( ; i <= 5; i++){
        	auto scoreLevel = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(LEVEL_SCORE, GameManager::getInstance()->getCurrentLevel(), i).c_str());
        	if(myScore >= scoreLevel){
        		break;
        	}
        }

        UserDefault::getInstance()->setIntegerForKey(StringUtils::format(LEVEL_SCORE, GameManager::getInstance()->getCurrentLevel(), i).c_str(), myScore);
        UserDefault::getInstance()->setIntegerForKey(StringUtils::format(LEVEL_STARS, GameManager::getInstance()->getCurrentLevel(), i).c_str(), stars);
        UserDefault::getInstance()->flush();

        SimpleAudioEngine::getInstance()->playEffect("magic_chime.wav");
        
        GameManager::getInstance()->setGameOverStars(stars);
        GameManager::getInstance()->setGameOverScore(myScore);

        auto gameOverLayer = GameOverLayer::createScene();
        this->addChild(gameOverLayer, zGameOver);
    }
    
    m_lbmfMovesMade->setString(StringUtils::format(LanguageManager::getInstance()->getStringForKey("countRun").c_str(), m_iMoveCounter, m_iNumPlays));
}

void MainLayer::MoveDisc(int idxA, int idxB, bool isPC){
    //m_maStacks.at(idxA), m_maStacks.at(idxB)
    DiscSprite *origen;

    origen = (DiscSprite*)m_maStacks.at(idxA).items.top();
    origen->retain();
    cocos2d::Point pos;
    
    if (m_maStacks.at(idxB).items.size() > 0) {
        if(!isPC) {
            Sprite *destino = (Sprite*)m_maStacks.at(idxB).items.top();
            if(destino->getContentSize().width < origen->getContentSize().width) return;
        }
        pos = Vec2(m_maStacks.at(idxB).topPos.x, m_maStacks.at(idxB).topPos.y + (DISCS_HEIGHT + DISCS_SEP));
    } else {
        if(m_maStacks.at(idxB).name.compare("stack1") == 0)
            pos = Vec2(BAR1_X, BARS_Y);
        else if(m_maStacks.at(idxB).name.compare("stack2") == 0)
            pos = Vec2(BAR2_X, BARS_Y);
        else if(m_maStacks.at(idxB).name.compare("stack3") == 0)
            pos = Vec2(BAR3_X, BARS_Y);
    }
    
    cocos2d::Point lptUp = Vec2(origen->myPos.x, m_iActionY);
    cocos2d::Point lptHor = Vec2(pos.x, m_iActionY);
    auto actionUp = MoveTo::create(0.3, lptUp);
    auto actionHor = MoveTo::create(0.4, lptHor);
    auto actionDown = MoveTo::create(0.3, pos);
    
    if(isPC) {
        auto action2 = CallFuncN::create(CC_CALLBACK_1(MainLayer::moveComplete, this));
        auto seq = Sequence::create(actionUp, actionHor, actionDown, action2, NULL);
        
        MovsOrder data;
        data.sprite = origen;
        data.sprite->retain();
        data.action = seq;
        data.action->retain();
        m_maMovsOrder.push_back(data);
    } else {
        
        auto action2 = CallFuncN::create(CC_CALLBACK_1(MainLayer::checkIfWon, this, m_maStacks.at(idxB).name, m_maStacks.at(idxB).items.size()));
        auto seq = Sequence::create(actionUp, actionHor, actionDown, action2, NULL);
        
        if(GameManager::getInstance()->getActiveSound()){
            SimpleAudioEngine::getInstance()->playEffect("discs.wav");
        }
        
        origen->runAction(seq);
    }
    
    origen->myPos = pos;
    m_maStacks.at(idxB).topPos = pos;
    
    m_maStacks.at(idxB).items.push(m_maStacks.at(idxA).items.top());
    m_maStacks.at(idxA).items.pop();
    
    m_iLastStackCount = m_maStacks.at(idxB).items.size();
    
    if(m_maStacks.at(idxA).items.size() > 0) {
        m_maStacks.at(idxA).topPos = ((DiscSprite*)m_maStacks.at(idxA).items.top())->myPos;
    } else {
        if(m_maStacks.at(idxA).name.compare("stack1") == 0)
            m_maStacks.at(idxA).topPos = Vec2(BAR1_X, BARS_Y);
        else if(m_maStacks.at(idxA).name.compare("stack2") == 0)
            m_maStacks.at(idxA).topPos = Vec2(BAR2_X, BARS_Y);
        else if(m_maStacks.at(idxA).name.compare("stack3") == 0)
            m_maStacks.at(idxA).topPos = Vec2(BAR3_X, BARS_Y);
    }
}

void MainLayer::MovePC(Ref *pSender){
    if(m_timeState == running) return;
    
    if(m_maStacks.at(0).items.size() != m_iNumDiscs)
        this->resetLevel(NULL);
    
    m_mMenu->setEnabled(false);
    m_speHelp->getActionManager()->pauseTarget(m_speHelp);
    
    for(int i = 0; i < m_mMenu->getChildrenCount(); i++){
        ((MenuItemSprite*)m_mMenu->getChildren().at(i))->setEnabled(false);
    }
    
    for (int x=1; x < (1 << m_iNumDiscs); x++) {
        int idxA = (x&x-1)%3;
        int idxB = ((x|x-1)+1)%3;
        this->MoveDisc(idxA, idxB, true);
    }
    
    this->schedule(CC_SCHEDULE_SELECTOR(MainLayer::tick), 1);
    
    m_timeState = running;
    m_bPlaysPC = true;
    m_iIdxOrden = 0;
    this->moveComplete(NULL);
}

void MainLayer::pauseGame(Ref* pSender){
    m_bIsPlayable = false;
    m_mMenu->setEnabled(false);

	m_speHelp->getActionManager()->pauseTarget(m_speHelp);
	this->getActionManager()->pauseTarget(this);

	auto pauseLayer = PauseLayer::createScene();

	this->addChild(pauseLayer, zGameOver);
}

void MainLayer::resetLevel(Ref* pSender){
	m_iStackSelected = 0;
	m_bIsPlayable = true;

	if(m_bPlaysPC || m_timeState == stopped){
		m_iMoveCounter = 0;
		m_iSeconds = 0;
		m_iCountDown = 3;
		m_timeState = stopped;

		m_speClockSec->setRotation(0);

		this->unschedule(CC_SCHEDULE_SELECTOR(MainLayer::tick));

		m_lbmfMovesMade->setString(StringUtils::format(LanguageManager::getInstance()->getStringForKey("countStop").c_str(),m_iNumPlays));
		m_lbmfClock->setString("00:00:00");
	}

	this->removeChildByTag(zSunRays, true);
	this->removeChildByTag(zSunHeart, true);
	this->removeChildByTag(zAz, true);
    
    while(m_skStack1.items.size() > 0) {
        m_skStack1.items.top()->removeFromParentAndCleanup(true);
        m_skStack1.items.pop();
    }
    
    while(m_skStack2.items.size() > 0) {
        m_skStack2.items.top()->removeFromParentAndCleanup(true);
        m_skStack2.items.pop();
    }
    
    while(m_skStack3.items.size() > 0) {
        m_skStack3.items.top()->removeFromParentAndCleanup(true);
        m_skStack3.items.pop();
    }
    
    m_maMovsOrder.erase(m_maMovsOrder.begin(),m_maMovsOrder.end());
    
    for (int i = m_iNumDiscs, j = 0; i >= 1; i--, j++) {
        auto sp = DiscSprite::create();
        sp->setSpriteFrame(StringUtils::format("%d.png", i));
        sp->setPosition(BAR1_X, BARS_Y + (DISCS_HEIGHT + DISCS_SEP) * j);
        sp->myPos = sp->getPosition();
        this->addChild(sp, zDiscs);
        m_skStack1.items.push(sp);
    }
    
    m_maStacks.erase(m_maStacks.begin(), m_maStacks.end());
    m_maStacks.push_back(m_skStack1);
    
    // Si la cantidad de discos es par se empieza moviendo a la barra 2...
    if(m_iNumDiscs%2 == 0){
        m_maStacks.push_back(m_skStack3);
        m_maStacks.push_back(m_skStack2);
    } else {
        m_maStacks.push_back(m_skStack2);
        m_maStacks.push_back(m_skStack3);
    }
}

void MainLayer::moveComplete(Ref *pSender){
    if(m_iIdxOrden < m_maMovsOrder.size()){
        if(GameManager::getInstance()->getActiveSound())
            SimpleAudioEngine::getInstance()->playEffect("discs.wav");
        
        m_maMovsOrder.at(m_iIdxOrden).sprite->runAction(m_maMovsOrder.at(m_iIdxOrden).action);
        m_iIdxOrden++;
        
        // PC siempre hace el minimo de jugadas posibles...
        if (m_iIdxOrden != 1) {
            m_iMoveCounter++;
            m_lbmfMovesMade->setString(StringUtils::format(LanguageManager::getInstance()->getStringForKey("countRun").c_str(), m_iMoveCounter, m_iNumPlays));
        }
    } else {
        m_iMoveCounter++;
        m_lbmfMovesMade->setString(StringUtils::format(LanguageManager::getInstance()->getStringForKey("countRun").c_str(), m_iMoveCounter, m_iNumPlays));
        this->tick(1);
        this->unschedule(CC_SCHEDULE_SELECTOR(MainLayer::tick));
        m_timeState = stopped;
        
        m_mMenu->setEnabled(true);
 
        m_speHelp->getActionManager()->resumeTarget(m_speHelp);
        
        for(int i = 0; i < m_mMenu->getChildrenCount(); i++){
            ((MenuItemSprite*)m_mMenu->getChildren().at(i))->setEnabled(true);
        }
    }
}

void MainLayer::countDownFinished(cocos2d::Ref *pSender){
    Size visibleSize = Director::getInstance()->getVisibleSize();
    cocos2d::Size halfVisibleSize = visibleSize;
    halfVisibleSize.width /= 2;
    halfVisibleSize.height /= 2;
    
    this->removeChild((Sprite*)pSender, true);
    
    if (m_iCountDown == 0) {
        m_timeState = running;
        m_bIsPlayable = true;
        m_mMenu->setEnabled(true);
        //self.isTouchEnabled = YES;
        this->schedule(CC_SCHEDULE_SELECTOR(MainLayer::tick), 1.0);
    } else {
        m_iCountDown--;
        
        string countDownString;
        if (m_iCountDown == 0)
            countDownString = StringUtils::format("ready_%s", GameManager::getInstance()->getLanguage().c_str());
        else
            countDownString = StringUtils::format("countDown%i", m_iCountDown);
        
        Sprite *countDownNumber = Sprite::createWithSpriteFrameName(countDownString);
        countDownNumber->setPosition(halfVisibleSize.width, 640 + 248);
        
        this->addChild(countDownNumber, zLabel);
        
        auto actMove = MoveTo::create(0.8, halfVisibleSize);
        auto actDelay = DelayTime::create(0.3);
        auto actMove2 = MoveTo::create(0.2, Vec2(halfVisibleSize.width, -248));
        auto actRemove = CallFuncN::create(CC_CALLBACK_1(MainLayer::countDownFinished, this));
        
        countDownNumber->runAction(Sequence::create(actMove, actDelay, actMove2, actRemove, NULL));
        
        if(GameManager::getInstance()->getActiveSound())
            SimpleAudioEngine::getInstance()->playEffect(m_iCountDown > 0 ? "counter.wav" : "counter_ready.wav");
    }
}

void MainLayer::playCallback(Node* pSender){
    //m_bIsPlayable = true;
    this->getActionManager()->resumeTarget(this);
}
void MainLayer::menuCallback(Node* pSender){
    SpriteFrameCache::getInstance()->removeSpriteFrames();
    
    if(GameManager::getInstance()->getActiveSound())
        SimpleAudioEngine::getInstance()->playEffect("magic_chime.wav");
    
    Director::getInstance()->replaceScene(TransitionFadeDown::create(TRANS_SPEED, MenuLayer::createScene()));
}
void MainLayer::levelsCallback(Node* pSender){
    SpriteFrameCache::getInstance()->removeSpriteFrames();
    
    if(GameManager::getInstance()->getActiveSound())
        SimpleAudioEngine::getInstance()->playEffect("magic_chime.wav");
    
    Director::getInstance()->replaceScene(TransitionFadeDown::create(TRANS_SPEED, LevelSelectionLayer::createScene()));
}
void MainLayer::nextCallback(Node* pSender){
    //m_bIsPlayable = true;
    
    SpriteFrameCache::getInstance()->removeSpriteFrames();
    
    if(m_iNumDiscs == 10) {
        if(GameManager::getInstance()->getActiveSound())
            SimpleAudioEngine::getInstance()->playEffect("magic_chime.wav");
        Director::getInstance()->replaceScene(TransitionFadeDown::create(TRANS_SPEED, LevelSelectionLayer::createScene()));
    } else {
        
        if(UserDefault::getInstance()->getIntegerForKey(StringUtils::format(LEVEL_STARS,GameManager::getInstance()->getCurrentLevel(),1).c_str()) > 0){
            GameManager::getInstance()->setCurrentLevel(GameManager::getInstance()->getCurrentLevel()+1);
            Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, MainLayer::createScene()));
        }else{
            if(GameManager::getInstance()->getActiveSound())
                SimpleAudioEngine::getInstance()->playEffect("magic_chime.wav");
            Director::getInstance()->replaceScene(TransitionFadeDown::create(TRANS_SPEED, LevelSelectionLayer::createScene()));
        }
    }
}
void MainLayer::resetCallback(Node* pSender){
    this->resetLevel(pSender);
}
void MainLayer::showHelp(){
    m_bIsPlayable = false;
	m_mMenu->setEnabled(false);

	m_speHelp->getActionManager()->pauseTarget(m_speHelp);
	this->getActionManager()->pauseTarget(this);

	auto helpLayer = HelpLayer::createScene();
	this->addChild(helpLayer, zGameOver);
}
/*
void MainLayer::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    _customCommand.init(2);
    _customCommand.func = CC_CALLBACK_0(MainLayer::onDraw, this, transform, flags);

    renderer->addCommand(&_customCommand);
}

void MainLayer::onDraw(const Mat4 &transform, uint32_t flags)
{
    Director* director = Director::getInstance();
    director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
    director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);

    //draw
    CHECK_GL_ERROR_DEBUG();

    // Check it poly
    DrawPrimitives::setDrawColor4B(255, 0, 0, 255);
    glLineWidth(2);

    Vec2 vertices[4] = {
    	m_ptFrame1.at(0),
		m_ptFrame1.at(1),
		m_ptFrame1.at(2),
		m_ptFrame1.at(3)
	};

    DrawPrimitives::drawPoly(vertices, 4, true);

    Vec2 vertices2[4] = {
		m_ptFrame2.at(0),
		m_ptFrame2.at(1),
		m_ptFrame2.at(2),
		m_ptFrame2.at(3)
	};

	DrawPrimitives::drawPoly(vertices2, 4, true);

	Vec2 vertices3[4] = {
		m_ptFrame3.at(0),
		m_ptFrame3.at(1),
		m_ptFrame3.at(2),
		m_ptFrame3.at(3)
	};

	DrawPrimitives::drawPoly(vertices3, 4, true);

    //end draw
    director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}
*/
