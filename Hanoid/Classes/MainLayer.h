#ifndef __MAIN_LAYER_H__
#define __MAIN_LAYER_H__

#include "cocos2d.h"
#include "DiscSprite.h"
#include <string>

using std::stack;
using std::string;
using namespace std;

typedef enum {
    stopped,
    running,
    paused
} timeState;

enum zOrderElems{
    zClouds = 1,
    zCylinders,
    zDiscs,
    zGround,
    zMenu,
    zLabel,
    zAz,
    zSunRays,
    zSunHeart,
    zGameOver
};

struct MovsOrder {
    DiscSprite* sprite;
    cocos2d::Action* action;
};

struct StackWithName {
    string name;
    stack<DiscSprite*> items;
    cocos2d::Point topPos;
};

class MainLayer : public cocos2d::Layer
{
private:
	int m_iNumDiscs, m_iNumPlays, m_iIdxOrden,
		m_iMoveCounter, m_iSeconds, m_iStackSelected,
		m_iCountDown,   m_iActionY, m_iLastStackCount;

	bool m_bPlaysPC;

	StackWithName m_skStack1, m_skStack2, m_skStack3;
    vector<StackWithName> m_maStacks;
    vector<MovsOrder> m_maMovsOrder;

	
	cocos2d::Label *m_lbmfMovesMade, *m_lbmfClock;
	cocos2d::Sprite *m_speSunHeart, *m_speSunRays, *m_speAz, *m_speClockSec;
	vector<cocos2d::Point> m_ptFrame1, m_ptFrame2, m_ptFrame3;
	
	timeState m_timeState;
    
public:
    bool m_bIsPlayable;
    cocos2d::Menu *m_mMenu;
    cocos2d::Sprite *m_speHelp;
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    virtual bool onTouchBegan(const cocos2d::Touch *touch, cocos2d::Event *unused_event);
    
    void tapDown(cocos2d::Vec2 location);
    void checkIfWon(Ref* pSender, string stackName, int stackSize);
    void MoveDisc(int idxA, int idxB, bool isPC);
    void tick(float dt);
    void createCloud(float dt);
    void cloudMoveFinished(Ref* pSender);
    bool PointInPolygon(cocos2d::Point point, vector<cocos2d::Point> points);
    void MoveManuallyToFrame(int frameNum);
    void showHelp();
    
    // a selector callback
    void MovePC(Ref* pSender);
    void pauseGame(Ref* pSender);
    void resetLevel(Ref* pSender);
    void moveComplete(Ref* pSender);
    void countDownFinished(Ref* pSender);
    
    void playCallback(Node* pSender);
    void menuCallback(Node* pSender);
    void levelsCallback(Node* pSender);
    void resetCallback(Node* pSender);
    void nextCallback(Node* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(MainLayer);
/*
	virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;

protected:
	void onDraw(const cocos2d::Mat4 &transform, uint32_t flags);
	cocos2d::CustomCommand _customCommand;
*/
};

#endif // __MAIN_LAYER_H__
