#include "MenuLayer.h"
#include "LevelSelectionLayer.h"
#include "OptionsLayer.h"
#include "HighscoresLayer.h"
#include "LanguageManager.h"
#include "GameManager.h"

USING_NS_CC;

Scene* MenuLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MenuLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MenuLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("menuButtons-hd.plist");

    // Background
    auto bkg = Sprite::create("background-hd.png");
    bkg->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(bkg, 0);

    // Buttons
    auto btnNormal = Sprite::createWithSpriteFrameName("buttonNormal");
    auto btnSize = btnNormal->getContentSize();
    auto marginRight = 25.0f;
	auto paddingMenu = 20.0f;
	auto fontSize = 50;
	//auto size = Size(270, 60);

	// Play button
	auto labelY = 355;
    auto playItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("buttonNormal"), Sprite::createWithSpriteFrameName("buttonOver"), CC_CALLBACK_1(MenuLayer::playCallback, this));

    auto playText = LanguageManager::getInstance()->getStringForKey("playText");

    auto playLabel = Label::createWithTTF(playText, FORTE_FONT, fontSize);
    playLabel->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight, labelY));
    playLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	this->addChild(playLabel, 10);

	auto playLabelShadow = Label::createWithTTF(playText, FORTE_FONT, fontSize);
	playLabelShadow->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight + 2, labelY - 2));
	playLabelShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	playLabelShadow->setColor( Color3B::BLACK );
	this->addChild(playLabelShadow, 9);
	//

	// Options button
    auto optionsItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("buttonNormal"), Sprite::createWithSpriteFrameName("buttonOver"), CC_CALLBACK_1(MenuLayer::optionsCallback, this));

    auto optionsText = LanguageManager::getInstance()->getStringForKey("optionsText");

    auto optionsLabel = Label::createWithTTF(optionsText, FORTE_FONT, fontSize);
    optionsLabel->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight, labelY - 110));
    optionsLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	this->addChild(optionsLabel, 10);

	auto optionsLabelShadow = Label::createWithTTF(optionsText, FORTE_FONT, fontSize);
	optionsLabelShadow->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight + 2, labelY - 110 - 2));
	optionsLabelShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	optionsLabelShadow->setColor( Color3B::BLACK );
	this->addChild(optionsLabelShadow, 9);
	//
	
	// Highscores button
    auto highscoresItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("buttonNormal"), Sprite::createWithSpriteFrameName("buttonOver"), CC_CALLBACK_1(MenuLayer::highscoresCallback, this));
	
    auto topScoresText = LanguageManager::getInstance()->getStringForKey("topScoresText");

    auto highscoresLabel = Label::createWithTTF(topScoresText, FORTE_FONT, fontSize);
    highscoresLabel->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight, labelY - 215));
    highscoresLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	this->addChild(highscoresLabel, 10);

	auto highscoresLabelShadow = Label::createWithTTF(topScoresText, FORTE_FONT, fontSize);
	highscoresLabelShadow->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight + 2, labelY - 215 - 2));
	highscoresLabelShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	highscoresLabelShadow->setColor( Color3B::BLACK );
	this->addChild(highscoresLabelShadow, 9);
	//

    auto menu = Menu::create(playItem, optionsItem, highscoresItem, NULL);
	menu->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight, 250.0f));
	menu->alignItemsVerticallyWithPadding(paddingMenu);
	this->addChild(menu, 1);

    return true;
}

void MenuLayer::makeNoise()
{
	if(GameManager::getInstance()->getActiveSound())
	{
		SimpleAudioEngine::getInstance()->playEffect("button.wav");
	}

	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("menuButtons-hd.plist");
}

void MenuLayer::playCallback(Ref* pSender)
{
	makeNoise();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, LevelSelectionLayer::createScene()));
}

void MenuLayer::optionsCallback(Ref* pSender)
{
	makeNoise();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, OptionsLayer::createScene()));
}

void MenuLayer::highscoresCallback(Ref* pSender)
{
	makeNoise();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, HighscoresLayer::createScene()));
}
