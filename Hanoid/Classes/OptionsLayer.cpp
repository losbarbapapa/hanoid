#include "OptionsLayer.h"
#include "MenuLayer.h"
#include "LanguageManager.h"
#include "GameManager.h"

USING_NS_CC;

Scene* OptionsLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = OptionsLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool OptionsLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // Sprite sheet Buttons
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("menuButtons-hd.plist", "menuButtons-hd.png");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("layerTitleIcons-hd.plist", "layerTitleIcons-hd.png");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("optionsIcons-hd.plist", "optionsIcons-hd.png");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("buttons-hd.plist", "buttons-hd.png");

    // Background
    auto bkg = Sprite::create("options_bkg-hd.png");
    bkg->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(bkg, 0);

    auto margin = 10;

    // Back button
	auto backButton = MenuItemSprite::create(Sprite::createWithSpriteFrameName("back"), Sprite::createWithSpriteFrameName("back_over"), CC_CALLBACK_1(OptionsLayer::backCallback, this));
	
	auto backBtnSize = backButton->getContentSize();

    auto menuBack = Menu::create(backButton, NULL);
    menuBack->setPosition(Vec2(backBtnSize.width/2 + margin, visibleSize.height - backBtnSize.width/2 - margin));
	this->addChild(menuBack, 1);

	int fontSize = 50;
	auto labelY = 355;

	// Sound button
	auto menuSound = MenuItemSprite::create(Sprite::createWithSpriteFrameName("buttonNormal"), Sprite::createWithSpriteFrameName("buttonOver"), CC_CALLBACK_1(OptionsLayer::soundCallback, this));

	auto soundText = LanguageManager::getInstance()->getStringForKey("soundText");

	auto btnSize = menuSound->getContentSize();
	auto marginRight = 25.0f;
	auto paddingMenu = 20.0f;
	auto X = visibleSize.width - btnSize.width/2 - marginRight;

	auto soundLabel = Label::createWithTTF(soundText, FORTE_FONT, fontSize);
	soundLabel->setPosition(Vec2(X, labelY));
	soundLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	this->addChild(soundLabel, 10);

	auto soundLabelShadow = Label::createWithTTF(soundText, FORTE_FONT, fontSize);
	soundLabelShadow->setPosition(Vec2(X + 2, labelY - 2));
	soundLabelShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	soundLabelShadow->setColor( Color3B::BLACK );
	this->addChild(soundLabelShadow, 9);

	auto soundIcon = Sprite::createWithSpriteFrameName((GameManager::getInstance()->getActiveSound() ? "soundOn" : "soundOff"));
	auto soundIconSize = soundIcon->getContentSize();
	soundIcon->setPosition(Vec2(visibleSize.width - soundIconSize.width/2 - 5, labelY));
	soundIcon->setRotation(-10);
	this->addChild(soundIcon, 30);
	//

	// Music button
	auto menuMusic = MenuItemSprite::create(Sprite::createWithSpriteFrameName("buttonNormal"), Sprite::createWithSpriteFrameName("buttonOver"), CC_CALLBACK_1(OptionsLayer::musicCallback, this));

	auto musicText = LanguageManager::getInstance()->getStringForKey("musicText");

	auto musicLabel = Label::createWithTTF(musicText, FORTE_FONT, fontSize);
	musicLabel->setPosition(Vec2(X, labelY - 110));
	musicLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	this->addChild(musicLabel, 10);

	auto musicLabelShadow = Label::createWithTTF(musicText, FORTE_FONT, fontSize);
	musicLabelShadow->setPosition(Vec2(X + 2, labelY - 110 - 2));
	musicLabelShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	musicLabelShadow->setColor( Color3B::BLACK );
	this->addChild(musicLabelShadow, 9);

	auto musicIcon = Sprite::createWithSpriteFrameName((GameManager::getInstance()->getActiveMusic() ? "musicOn" : "musicOff"));
	auto musicIconSize = musicIcon->getContentSize();
	musicIcon->setPosition(Vec2(visibleSize.width - musicIconSize.width/2 - 5, labelY - 110));
	musicIcon->setRotation(-10);
	this->addChild(musicIcon, 30);
	//

	// Language button
	auto menuLang = MenuItemSprite::create(Sprite::createWithSpriteFrameName("buttonNormal"), Sprite::createWithSpriteFrameName("buttonOver"), CC_CALLBACK_1(OptionsLayer::languageCallback, this));

	auto languageText = LanguageManager::getInstance()->getStringForKey("languageText");

	auto langLabel = Label::createWithTTF(languageText, FORTE_FONT, fontSize);
	langLabel->setPosition(Vec2(X, labelY - 215));
	langLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	this->addChild(langLabel, 10);

	auto langLabelShadow = Label::createWithTTF(languageText, FORTE_FONT, fontSize);
	langLabelShadow->setPosition(Vec2(X + 2, labelY - 215 - 2));
	langLabelShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	langLabelShadow->setColor( Color3B::BLACK );
	this->addChild(langLabelShadow, 9);

	auto langIcon = Sprite::createWithSpriteFrameName((GameManager::getInstance()->getLanguage()));
	auto langIconSize = langIcon->getContentSize();
	langIcon->setPosition(Vec2(visibleSize.width - langIconSize.width/2 - 5, labelY - 215));
	langIcon->setRotation(-10);
	this->addChild(langIcon, 30);
	//

	// Menu
	auto menu = Menu::create(menuSound, menuMusic, menuLang, NULL);
	menu->setPosition(Vec2(visibleSize.width - btnSize.width/2 - marginRight, 250.0f));
	menu->alignItemsVerticallyWithPadding(paddingMenu);
	this->addChild(menu, 1);
	//

	auto keyboardListener = cocos2d::EventListenerKeyboard::create();
	keyboardListener->onKeyReleased = CC_CALLBACK_2(OptionsLayer::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

    return true;
}

void OptionsLayer::makeNoise()
{
	if(GameManager::getInstance()->getActiveSound())
	{
		SimpleAudioEngine::getInstance()->playEffect("button.wav");
	}
}

void OptionsLayer::soundCallback(Ref* pSender)
{
	if(GameManager::getInstance()->getActiveSound()){
		SimpleAudioEngine::getInstance()->playEffect("button.wav");
		GameManager::getInstance()->setActiveSound(false);
		UserDefault::getInstance()->setIntegerForKey(ACTIVE_SOUND, 0);
	}else{
		SimpleAudioEngine::getInstance()->playEffect("magic_chime.wav");
		GameManager::getInstance()->setActiveSound(true);
		UserDefault::getInstance()->setIntegerForKey(ACTIVE_SOUND, 1);
	}

	UserDefault::getInstance()->flush();
	Director::getInstance()->replaceScene(TransitionFade::create(0, OptionsLayer::createScene()));
}

void OptionsLayer::musicCallback(Ref* pSender)
{
	makeNoise();

	if(GameManager::getInstance()->getActiveMusic()){
		GameManager::getInstance()->setActiveMusic(false);
		SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		UserDefault::getInstance()->setIntegerForKey(ACTIVE_MUSIC, 0);
	}else{
		GameManager::getInstance()->setActiveMusic(true);
		SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		UserDefault::getInstance()->setIntegerForKey(ACTIVE_MUSIC, 1);
	}

	UserDefault::getInstance()->flush();
	Director::getInstance()->replaceScene(TransitionFade::create(0, OptionsLayer::createScene()));
}

void OptionsLayer::languageCallback(Ref* pSender)
{
	makeNoise();

	string language = GameManager::getInstance()->getLanguage();
	if(strcmp(language.c_str(), "en") == 0){
		GameManager::getInstance()->setLanguage("es");
		LanguageManager::getInstance()->setLanguage("es");
	}else{
		GameManager::getInstance()->setLanguage("en");
		LanguageManager::getInstance()->setLanguage("en");
	}

	UserDefault::getInstance()->setStringForKey("language", GameManager::getInstance()->getLanguage());
	UserDefault::getInstance()->flush();

	Director::getInstance()->replaceScene(TransitionFade::create(0, OptionsLayer::createScene()));
}

void OptionsLayer::backCallback(Ref* pSender)
{
	makeNoise();

	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("menuButtons-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("layerTitleIcons-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("optionsIcons-hd.plist");
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("buttons-hd.plist");

	Director::getInstance()->replaceScene(TransitionFade::create(TRANS_SPEED, MenuLayer::createScene()));
}

void OptionsLayer::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
	{
		this->backCallback(NULL);
	}
}
