#ifndef __OPTIONS_LAYER_H__
#define __OPTIONS_LAYER_H__

#include "cocos2d.h"

class OptionsLayer : public cocos2d::Layer
{
private:
	void makeNoise();

public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void soundCallback(cocos2d::Ref* pSender);
    void languageCallback(cocos2d::Ref* pSender);
    void musicCallback(cocos2d::Ref* pSender);
    void backCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(OptionsLayer);

    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
};

#endif // __OPTIONS_LAYER_H__
