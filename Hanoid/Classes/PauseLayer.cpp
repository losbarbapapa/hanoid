#include "PauseLayer.h"
#include "MenuLayer.h"
#include "MainLayer.h"
#include "GameManager.h"
#include "LanguageManager.h"

USING_NS_CC;

Scene* PauseLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PauseLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PauseLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 128)))
    {
        return false;
    }
    
    if(GameManager::getInstance()->getActiveMusic()){
    	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    	m_iBkgMusicHandle = SimpleAudioEngine::getInstance()->playEffect("game_paused.wav", true);
    }
    
	cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	cocos2d::Size halfVisibleSize = visibleSize;
    halfVisibleSize.width /= 2;
    halfVisibleSize.height /= 2;

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("dialogs-hd.plist");
    
	// Background
    auto bkg = Sprite::createWithSpriteFrameName("cloud");
    bkg->setPosition(Vec2(halfVisibleSize.width, halfVisibleSize.height));
    this->addChild(bkg);
	
    // Play level
    auto playSprite = Sprite::createWithSpriteFrameName("play");
    auto playOverSprite = Sprite::createWithSpriteFrameName("play_over");

    auto playText = LanguageManager::getInstance()->getStringForKey("playText");
    auto labelPlay = Label::createWithTTF(playText, FORTE_FONT, 60);
	labelPlay->setPosition(halfVisibleSize.width + 2, halfVisibleSize.height + 20 - 2);
    labelPlay->setColor(Color3B::BLACK);
    this->addChild(labelPlay, 10);
  
    labelPlay = Label::createWithTTF(playText, FORTE_FONT, 60);
    labelPlay->setPosition(halfVisibleSize.width - 2, halfVisibleSize.height + 20 + 2);
    labelPlay->setColor(Color3B::BLACK);
    this->addChild(labelPlay, 10);
    
    labelPlay = Label::createWithTTF(playText, FORTE_FONT, 60);
    labelPlay->setPosition(halfVisibleSize.width, halfVisibleSize.height + 20);
    labelPlay->setColor(Color3B(0, 202, 0));
    this->addChild(labelPlay, 10);

    // Main menu
    Sprite *menuSprite = Sprite::createWithSpriteFrameName("main_menu");
    Sprite *menuOverSprite = Sprite::createWithSpriteFrameName("main_menu_over");

    auto menuText = LanguageManager::getInstance()->getStringForKey("menuText");
    Label *labelMenu = Label::createWithTTF(menuText, FORTE_FONT, 30);
    labelMenu->setPosition(halfVisibleSize.width - 200 + 2, halfVisibleSize.height - 130 - 2);
    labelMenu->setColor(Color3B::BLACK);
    this->addChild(labelMenu, 10);
    
    labelMenu = Label::createWithTTF(menuText, FORTE_FONT, 30);
    labelMenu->setPosition(halfVisibleSize.width - 200 - 2, halfVisibleSize.height - 130 + 2);
    labelMenu->setColor(Color3B::BLACK);
    this->addChild(labelMenu, 10);
    
    labelMenu = Label::createWithTTF(menuText, FORTE_FONT, 30);
    labelMenu->setPosition(halfVisibleSize.width - 200, halfVisibleSize.height - 130);
    labelMenu->setColor(Color3B(0, 202, 0));
    this->addChild(labelMenu, 10);

    // Select level
    Sprite *levelsSprite = Sprite::createWithSpriteFrameName("level_select");
    Sprite *levelsOverSprite = Sprite::createWithSpriteFrameName("level_select_over");

    auto levelsText = LanguageManager::getInstance()->getStringForKey("levelsText");
    Label *labelLevels = Label::createWithTTF(levelsText, FORTE_FONT, 30);
    labelLevels->setPosition(halfVisibleSize.width + 200 + 2, halfVisibleSize.height - 130 - 2);
    labelLevels->setColor(Color3B::BLACK);
    this->addChild(labelLevels, 10);
    
    labelLevels = Label::createWithTTF(levelsText, FORTE_FONT, 30);
    labelLevels->setPosition(halfVisibleSize.width + 200 - 2, halfVisibleSize.height - 130 + 2);
    labelLevels->setColor(Color3B::BLACK);
    this->addChild(labelLevels, 10);
    
    labelLevels = Label::createWithTTF(levelsText, FORTE_FONT, 30);
    labelLevels->setPosition(halfVisibleSize.width + 200, halfVisibleSize.height - 130);
    labelLevels->setColor(Color3B(0, 202, 0));
    this->addChild(labelLevels, 10);


    // Next button
    Sprite *nextSprite = Sprite::createWithSpriteFrameName(StringUtils::format("next_%s", GameManager::getInstance()->getLanguage().c_str()));
    Sprite *nextOverSprite = Sprite::createWithSpriteFrameName(StringUtils::format("next_over_%s", GameManager::getInstance()->getLanguage().c_str()));

    MenuItemSprite *playButton = MenuItemSprite::create(playSprite, playOverSprite, CC_CALLBACK_1(PauseLayer::playCallback, this));
    playButton->setPosition(halfVisibleSize.width, halfVisibleSize.height + 140);

    MenuItemSprite *menuButton = MenuItemSprite::create(menuSprite, menuOverSprite, CC_CALLBACK_1(PauseLayer::menuCallback, this));
    menuButton->setPosition(halfVisibleSize.width - 200, halfVisibleSize.height - 60);

    MenuItemSprite *levelsButton = MenuItemSprite::create(levelsSprite, levelsOverSprite, CC_CALLBACK_1(PauseLayer::levelsCallback, this));
    levelsButton->setPosition(halfVisibleSize.width + 200, halfVisibleSize.height - 60);

    MenuItemSprite *nextButton = MenuItemSprite::create(nextSprite, nextOverSprite, CC_CALLBACK_1(PauseLayer::nextCallback, this));
    nextButton->setPosition(halfVisibleSize.width, halfVisibleSize.height - 220);

    Menu *menu = Menu::create(playButton, menuButton, levelsButton, nextButton, NULL);
    menu->setPosition(0,0);
    this->addChild(menu);


	return true;
}

void PauseLayer::resumeMusicAndExit()
{
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("dialogs-hd.plist");
	this->removeFromParentAndCleanup(true);
	if(GameManager::getInstance()->getActiveMusic()){
		SimpleAudioEngine::getInstance()->stopEffect(m_iBkgMusicHandle);
		SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	}
}

void PauseLayer::playCallback(Ref* pSender){
    auto main = (MainLayer*)Director::getInstance()->getRunningScene()->getChildByName("MainLayer");
    main->m_bIsPlayable = true;
    main->m_mMenu->setEnabled(true);
    main->m_speHelp->getActionManager()->resumeTarget(main->m_speHelp);
    
    auto play = CallFuncN::create(this, callfuncN_selector(MainLayer::playCallback));
    play->execute();
    
	resumeMusicAndExit();
}

void PauseLayer::menuCallback(Ref* pSender){
    
    auto menu = CallFuncN::create(this, callfuncN_selector(MainLayer::menuCallback));
    menu->execute();
    
	resumeMusicAndExit();
}

void PauseLayer::levelsCallback(Ref* pSender){
    
    auto levels = CallFuncN::create(this, callfuncN_selector(MainLayer::levelsCallback));
    levels->execute();
    
	resumeMusicAndExit();
}

void PauseLayer::nextCallback(Ref* pSender){
    auto main = (MainLayer*)Director::getInstance()->getRunningScene()->getChildByName("MainLayer");
    main->m_bIsPlayable = true;
    main->m_mMenu->setEnabled(true);
    main->m_speHelp->getActionManager()->resumeTarget(main->m_speHelp);
    
    auto next = CallFuncN::create(this, callfuncN_selector(MainLayer::nextCallback));
    next->execute();
    
	resumeMusicAndExit();
}