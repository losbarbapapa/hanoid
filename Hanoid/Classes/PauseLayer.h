#ifndef __PAUSE_LAYER_H__
#define __PAUSE_LAYER_H__

#include "cocos2d.h"

class PauseLayer : public cocos2d::LayerColor
{
private:
    int m_iBkgMusicHandle;
    void resumeMusicAndExit();
    
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // a selector callback
    void playCallback(Ref* pSender);
	void menuCallback(Ref* pSender);
	void levelsCallback(Ref* pSender);
	void nextCallback(Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(PauseLayer);
};

#endif // __PAUSE_LAYER_H__
